// Create a variable for the API endpoint. In this example, we're accessing Xano's API
let xanoUrl = new URL('https://x039-8d94-9774.dev.xano.io/api:FFH5FMYr/items_of_member');

// Define a function (set of operations) to get item information.
// This will use the GET request on the URL endpoint
function getItems() {

		let webflowMemberID = document.getElementById("Webflow-Member-ID")
        xanoUrl.searchParams.set("webflow_member_id", webflowMemberID.innerText);

    // Create a request variable and assign a new XMLHttpRequest object to it.
    // XMLHttpRequest is the standard way you access an API in plain Javascript.
    let request = new XMLHttpRequest();

    // Define a function (set of operations) to get item information.
    // Creates a variable that will take the URL from above and makes sure it displays as a string. 
    // We then add the word 'item" so the API endpoint becomes https://x715-fe9c-6426.n7.xano.io/api:Iw1iInWB/item
    let url = xanoUrl.toString();


    // Remember the 'request' was defined above as the standard way to access an API in Javascript.
    // GET is the verb we're using to GET data from Xano
    request.open('GET', url, true)

    // When the 'request' or API request loads, do the following...
    request.onload = function() {

        // Store what we get back from the Xano API as a variable called 'data' and converts it to a javascript object
        let data = JSON.parse(this.response)

        // Status 200 = Success. Status 400 = Problem.  This says if it's successful and no problems, then execute 
        if (request.status >= 200 && request.status < 400) {

            // Map a variable called cardContainer to the Webflow element called "Cards-Container"
            const cardContainer = document.getElementById("Cards-Container")

            // This is called a For Loop. This goes through each object being passed back from the Xano API and does something.
            // Specifically, it says "For every element in Data (response from API), call each individual item item"
            data.forEach(item => {

                // For each item, create a div called card and style with the "Sample Card" class
                const card = document.createElement('div')
                card.setAttribute('class', 'sample-card')

                // For each item, Create an image and use the item image coming from the API
                const img = document.createElement('img');
                img.src = item.banner.url + '?tpl=big:box ';

                // For each item, create an h3 and set the text content to the item's title
                const h3 = document.createElement('h3');
                h3.textContent = item.name;
                // For each item, create an paragraph and set the text content to the item's description
                const p = document.createElement('p');
                p.textContent = `${item.description.substring(0, 240)}` // Limit to 240 chars

                // Attach the img, h3 and p to the item card
                card.appendChild(img);
                card.appendChild(h3);
                card.appendChild(p);

                // Place the card into the div "Cards-Container" 
                cardContainer.appendChild(card);
            })
        }
    }

    // Send item request to API
    request.send();
}



// This fires all of the defined functions when the document is "ready" or loaded
(function() {
		setTimeout(() => {
    getItems();
 }, 250)
})();
